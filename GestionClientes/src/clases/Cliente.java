package clases;

public class Cliente {
	private Factura[] facturas;
	
	public Cliente(int maxFacturas) {
		this.facturas = new Factura[maxFacturas];
	}
	
	public void altaFactura(String codFactura, double importe, String codCliente, String concepto, int cantidad){
		for (int i = 0; i < facturas.length; i++) {
			if (facturas[i] == null) {
				facturas[i] = new Factura(codFactura);
				facturas[i].setImporte(importe);
				facturas[i].setCodCliente(codCliente);
				facturas[i].setConcepto(concepto);
				facturas[i].setCantidad(cantidad);
				break;
			}
		}
	}
	
	public Factura buscarFactura(String codFactura) {
		for (int i = 0; i < facturas.length; i++) {
			if (facturas[i] != null) {
				if (facturas[i].getCodFactura().equals(codFactura)) {
					return facturas[i];
				}
			}
		}
		return null;
	}
	
	public void eliminarFactura(String codFactura) {
		for (int i = 0; i < facturas.length; i++) {
			if (facturas[i] != null) {
				if (facturas[i].getCodFactura().equals(codFactura)) {
					facturas[i]=null;
				}
			}
		}
	}
	
	public void listarFacturas() {
		for (int i = 0; i < facturas.length; i++) {
			if (facturas[i] != null) {
					System.out.println(facturas[i]);				
			}
		}
	}	
}
