package clases;

public class Factura {
	
	private String codFactura;
	private double importe;
	private String codCliente;
	private String concepto;
	private int cantidad;
	
	public Factura(String codFactura) {
		this.codFactura = codFactura;
	}
	
	public String getCodFactura() {
		return codFactura;
	}
	public void setCodFactura(String codFactura) {
		this.codFactura = codFactura;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public String getCodCliente() {
		return codCliente;
	}
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	@Override
	public String toString(){
		return "CodFactura:"+codFactura 
				+ " Importe:" + importe 
				+ " Cliente:" + codCliente
				+ " Concepto:" + concepto
				+ " Cantidad:" + cantidad ;
	}
	
	
}
