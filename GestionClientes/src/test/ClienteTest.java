package test;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;

public class ClienteTest {

	static Cliente cliente1;

	@BeforeClass
	public static void setUp() {
		// creo un cliente con dos facturas
		cliente1=new Cliente(2);
	}

	@Test
	public void testAltaFactura1() {
		// doy de alta una factura y la busco
		cliente1.altaFactura("F1501", 250.25, "Cliente1","Camisetas",15);
		String esperado = "F1501";
		String respuesta = cliente1.buscarFactura("F1501").getCodFactura();	
		assertEquals(esperado,respuesta );
		
		System.out.println(respuesta);
		assertEquals(esperado, respuesta);
	}
	
	@Test
	public void testAltaFactura2() {
		// doy de alta dos facturas y busco una no esperada
		cliente1.altaFactura("F1501", 250.25, "Cliente1","Camisetas",15);
		cliente1.altaFactura("F1502", 340.35, "Cliente2", "Pantalones",20);
		String esperado = "F1502";
		String respuesta = cliente1.buscarFactura("F1501").getCodFactura();
		assertNotEquals(esperado, respuesta);
	}
	
	@Test
	public void testAltaFactura3() {
		// doy de alta una factura que no puedo (maximo 2) y busco
		cliente1.altaFactura("F1503", 1200.15, "Cliente1","zapatos",25);
		Factura esperado = cliente1.buscarFactura("F1503");
		assertNull(esperado);
	}

	@Test
	public void testEliminarAlbaran1() {
		// doy de alta una factura, la elimino  y compruebo que no esta
		cliente1.altaFactura("F1501", 250.25, "Cliente1","Camisetas",15);
		cliente1.eliminarFactura("F1501");
		assertNull(cliente1.buscarFactura("F1501"));
	}
	
	// elimino una factura
	@Test
	public void testEliminarAlbaran2() {
		// doy de alta dos facturas, elimino una  y compruebo que no es la eliminada
		cliente1.altaFactura("F1501", 250.25, "Cliente1","Camisetas",15);
		cliente1.altaFactura("F1502", 340.35, "Cliente2", "Pantalones",20);
		cliente1.eliminarFactura("F1501");
		assertNotNull(cliente1.buscarFactura("F1502"));
	}

}
