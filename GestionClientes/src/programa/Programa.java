package programa;

import clases.Cliente;

public class Programa {

	public static void main(String[] args) {		
		System.out.println("Crear una instancia de Cliente con 2 facturas");
		Cliente clientela = new Cliente(2);
		System.out.println("Instancia creada");
		System.out.println("");

		System.out.println("Damos de alta 2 facturas (F1200-F1201)");
		clientela.altaFactura("F1200", 250.25, "Cliente1","Camisetas",15);
		clientela.altaFactura("F1201", 340.35, "Cliente2", "Pantalones",20);
		clientela.listarFacturas();
		System.out.println("");
		
		System.out.println("Buscamos factura (F1200)");
		System.out.println(clientela.buscarFactura("F1200"));
		System.out.println("");
		
		System.out.println("Eliminar factura (F1201)");
		clientela.eliminarFactura("F1201");
		clientela.listarFacturas();
		}
}
