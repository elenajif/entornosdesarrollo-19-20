package primeraPrueba;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class PrimeraPrueba {

	private JFrame frame;
	private JTextField textRadio;
	private JTextField textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrimeraPrueba window = new PrimeraPrueba();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PrimeraPrueba() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblRadio = new JLabel("Radio");
		lblRadio.setBounds(92, 58, 46, 14);
		frame.getContentPane().add(lblRadio);
		
		textRadio = new JTextField();
		textRadio.setBounds(137, 55, 91, 20);
		frame.getContentPane().add(textRadio);
		textRadio.setColumns(10);
		
		JLabel lblArea = new JLabel("Area");
		lblArea.setBounds(92, 100, 46, 14);
		frame.getContentPane().add(lblArea);
		
		textArea = new JTextField();
		textArea.setEditable(false);
		textArea.setForeground(Color.BLACK);
		textArea.setBounds(137, 97, 236, 20);
		frame.getContentPane().add(textArea);
		textArea.setColumns(10);
		
		JButton calcularArea = new JButton("Calcular Area");
		calcularArea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double radio=Double.parseDouble(textRadio.getText());
				double area=Math.PI*radio*radio;
				textArea.setText(String.valueOf(area));	
			}
		});
		calcularArea.setBounds(134, 167, 135, 23);
		frame.getContentPane().add(calcularArea);
	}
}
