package test;

import static org.junit.Assert.*;

import org.junit.Test;

import clases.UsandoVectores;

public class UsandoVectoresTest {

	@Test
	public void testSumaVector() {
		System.out.println("Test usando vectores suma");
		UsandoVectores miSuma = new UsandoVectores();
		Double miVector[] = {4.0,5.0,6.0,3.3,5.2};
		double esperado=23.5;
		double resultado=miSuma.sumaVector(miVector);
		assertEquals(esperado,resultado,0.00001);
		// double x=2
		// double y=3
		// esperado x+y=5
		// suma -> 5.000000000001
		// esperado y suma no ser�an iguales, pasara eso usamos delta
	}
	
	@Test
	public void testMinimoVector() {
		System.out.println("Test usando vectores minimo");
		UsandoVectores miMinimo = new UsandoVectores();
		Double miVector[] = {4.0,5.0,6.0,3.3,5.2};
		double noEsperado=3;
		double resultado=miMinimo.minimoVector(miVector);
		assertNotEquals(noEsperado,resultado,0.0001);
	}
	
	
}
