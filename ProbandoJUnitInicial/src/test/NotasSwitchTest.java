package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import clases.NotasSwitch;

public class NotasSwitchTest {

	static NotasSwitch miNota;
	
	@Before
	public void setUpBeforeTest() {
		miNota=new NotasSwitch();
	}
	
	
	@Test
	public void testNotas1() {
		System.out.println("Test1 usando notas1");
		// NotasSwitch miNota = new NotasSwitch();
		int nota=3;	
		String esperado="insuficiente";
		String resultado=miNota.notas(nota);
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testNotas2() {
		System.out.println("Test2 usando notas1");
		// NotasSwitch miNota = new NotasSwitch();
		int nota=15;	
		String esperado="La opcion introducida no es valida";
		String resultado=miNota.notas(nota);
		assertEquals(esperado, resultado);
	}
	

}
