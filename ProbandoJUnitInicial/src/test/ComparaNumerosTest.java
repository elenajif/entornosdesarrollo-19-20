package test;

import static org.junit.Assert.*;

import org.junit.Test;

import clases.ComparaNumeros;

public class ComparaNumerosTest {

	@Test
	public void numeroMayorTest1() {
		System.out.println("Test1 numero mayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado=0;
		int resultado=compara.numeroMayor(0, 0, 0);
		assertEquals(esperado,resultado);
	}

	@Test
	public void numeroMayorTest2() {
		System.out.println("Test2 numero mayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado=7;
		int resultado=compara.numeroMayor(4, 5, 7);
		assertEquals(esperado,resultado);
	}
	
	@Test
	public void numeroMayorTest3() {
		System.out.println("Test3 numero mayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado=-2;
		int resultado=compara.numeroMayor(-2, -3, -5);
		assertEquals(esperado,resultado);
	}
	
	@Test
	public void numeroMayorTest4() {
		System.out.println("Test4 numero mayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado=5;
		int resultado=compara.numeroMayor(5, 8, 7);
		assertNotEquals(esperado,resultado);
	}
}
