package test;

import static org.junit.Assert.*;

import org.junit.Test;

import clases.UsandoVectores;


	public class UsandoVectoresTest {
		@Test
		public void testSumaVector() {
			System.out.println("test usando vectores suma");
			UsandoVectores miSuma = new UsandoVectores();
			Double miVector[]={4.0,5.0,6.0,3.3,5.2};
			double esperado=23.5;
			double resultado=miSuma.sumaVector(miVector);
			assertEquals(esperado, resultado,0.0000001);
			//2+5
			//7
			//7.0000000001
		}
		@Test
		public void testMinimoVector() {
			System.out.println("test usando vectores minimo");
			UsandoVectores miMinimo = new UsandoVectores();
			Double miVector[]={4.0,5.0,6.0,3.3,5.2};
			double noEsperado=3;
			double resultado=miMinimo.minimoVector(miVector);
			assertNotEquals(noEsperado, resultado,0.0000001);
		}
	}
