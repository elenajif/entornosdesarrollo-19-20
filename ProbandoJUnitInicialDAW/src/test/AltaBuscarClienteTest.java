package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class AltaBuscarClienteTest {

	static GestorContabilidad gestor;
	@BeforeClass
	public static void setUpBeforeClass() {
		gestor=new GestorContabilidad();
	}
	
	@Before
	public void setUpBeforeTest() {
		gestor.getListaClientes().clear();
		gestor.getListaFacturas().clear();
	}
	
	@Test
	public void altaClienteConCeroCliente() {
		Cliente esperado = new Cliente("1234R");
		gestor.altaCliente(esperado);
		assertTrue(gestor.getListaClientes().contains(esperado));
	}
	
	@Test
	public void altaClienteConVariosClientesAnadidos() {
		Cliente cliente1 = new Cliente("4576U");
		gestor.getListaClientes().add(cliente1);
		Cliente cliente2 = new Cliente("4234G");
		gestor.getListaClientes().add(cliente2);
		
		Cliente esperado = new Cliente("1234R");
		gestor.altaCliente(esperado);
		
		boolean resultado = gestor.getListaClientes().contains(esperado);
		assertTrue(resultado);
		
	}
	

}
