package test;

import static org.junit.Assert.*;

import org.junit.Test;

import clases.UsandoVectores;

public class UsandoVectoresTest {

	@Test
	public void testSumaVector() {
		System.out.println("Test usando vectores suma");
		UsandoVectores miSuma = new UsandoVectores();
		Double miVector[] = {4.0,5.0,6.0,3.3,5.2};
		double esperado=23.5;
		double resultado=miSuma.sumaVector(miVector);
		assertEquals(esperado, resultado, 0.001);
	}
	@Test
	public void testMinimoVector() {
		System.out.println("Test usando vectores minimo");
		UsandoVectores miMinimo = new UsandoVectores();
		Double miVector[] = {4.0,5.0,6.0,3.3,5.2};
		double NoEsperado=3;
		double resultado=miMinimo.minimoVector(miVector);
		assertNotEquals(NoEsperado, resultado, 0.001);
	}

}

