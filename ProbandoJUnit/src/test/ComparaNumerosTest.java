package test;

import static org.junit.Assert.*;

import org.junit.Test;

import clases.ComparaNumeros;

public class ComparaNumerosTest {

	@Test
	public void NumeroMayorTest1() {
		System.out.println("Test1 numeroMayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado = 0;
		int resultado = compara.numeroMayor(0, 0, 0);
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void NumeroMayorTest2() {
		System.out.println("Test2 numeroMayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado = 7;
		int resultado = compara.numeroMayor(4, 5, 7);
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void NumeroMayorTest3() {
		System.out.println("Test3 numeroMayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado = -2;
		int resultado = compara.numeroMayor(-2, -3, -5);
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void NumeroMayorTest4() {
		System.out.println("Test4 numeroMayor");
		ComparaNumeros compara = new ComparaNumeros();
		int noEsperado = 5;
		int resultado = compara.numeroMayor(5, 8, 7);
		assertNotEquals(noEsperado, resultado);
	}

}
